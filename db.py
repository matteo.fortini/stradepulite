import strada
import json
import datetime
import logging
from watchdog.events import FileSystemEventHandler
from watchdog.observers.api import BaseObserver, EventEmitter
from threading import Lock

class DB(FileSystemEventHandler):
    def __init__(self, path):
        self.lock = Lock()
        self.path = path
        self.__read_file()
        self.observer = BaseObserver(EventEmitter)
        self.observer.schedule(self, path)
        self.observer.start()

    def __read_file(self):
        self.lock.acquire()
        print("loading file")

        self.__strade = []
        self.__byName = {}
        self.__byMonthDay = {}
        self.__byRealDate = {}

        with open(self.path, 'r') as f:
            jd = json.load(f)
            for o in jd:
                s = strada.strada.from_dict(o)
                self.__strade.append(s)
                if s.nome in self.__byName:
                    self.__byName[s.nome].append(s)
                else:
                    self.__byName[s.nome] = [s]
                if s.monthday in self.__byMonthDay:
                    self.__byMonthDay[s.monthday].append(s)
                else:
                    self.__byMonthDay[s.monthday] = [s]
                if s.next_real_day() in self.__byRealDate:
                    self.__byRealDate[s.next_real_day()].append(s)
                else:
                    self.__byRealDate[s.next_real_day()] = [s]
        self.lock.release()

    def length(self):
        self.lock.acquire()
        s = len(self.__strade)
        self.lock.release()
        return s

    def strade(self):
        self.lock.acquire()
        s = self.__strade[:]
        self.lock.release()
        return s

    def names(self):
        self.lock.acquire()
        s = self.__byName.keys()
        self.lock.release()
        return s

    def byName(self, nome):
        self.lock.acquire()
        s = self.__byName[nome]
        self.lock.release()
        return s

    def byMonthDay(self, day):
        self.lock.acquire()
        s = self.__byMonthDay[day]
        self.lock.release()
        return s

    def byRealDate(self, date):
        self.lock.acquire()
        s = self.__byRealDate[date]
        self.lock.release()
        return s

    def on_modified(self, event):
        self.__read_file()


    def print_next_day(self, start_day = datetime.date.today()):
        self.lock.acquire()
        while start_day not in self.__byRealDate:
            start_day += datetime.timedelta(days=1)

        if start_day == datetime.date.today():
            print("prissimo giorno con lavaggio: OGGI\nstrade:\n")
        else:
            print("prissimo giorno con lavaggio: {}\nstrade:\n".format(start_day))
        for s in self.__byRealDate[start_day]:
            print(s)
        self.lock.release()


