#!/usr/bin/env python3

import json
from bs4 import BeautifulSoup
import urllib.request as httpreq
import sys
import os
import strada

store_file = "strade_db.json"
remote_url = "http://www.gruppohera.it/statico/bologna/db_pulizia_strade.php"

db = []
if os.path.isfile(store_file):
    sf = open(store_file, 'r')
    db = [strada.strada.from_dict(x) for x in json.load(sf)]
    sf.close()

new_items = []
for o in BeautifulSoup(httpreq.urlopen(remote_url).read(), "html.parser").find("form", {"id":"pulizia_strade"}).find("select").findAll("option"):
    s = BeautifulSoup(httpreq.urlopen("{}?via={}".format(remote_url, o['value'])).read(), "html.parser")
    try:
        nome = s.find("div", "selezionato").strong.text
        lines = s.ul.find_all("li")
        for l in lines:
            cont = l.strong
            quale = cont.text
            giorno = cont.next_sibling.text
            try:
                descrizione_alt = str(cont.next_sibling.next_sibling.next_sibling)
            except:
                descrizione_alt = ""
            street = strada.strada(o['value'], nome, quale, giorno, descrizione_alt)
            if street not in db:
                new_items.append(street)
                db.append(street)
    except Exception as e:
        print("error parsing element {} ({}): {}".format(o.text, o['value'], e), file=sys.stderr)

if len(new_items) > 0:
    print("NOVITA`:")
    for s in new_items:
        print(s.nome)

sorted(db, key=lambda s: s.nome)

sf = open(store_file, 'w')
sf.write(json.dumps([x.dict() for x in db]))
sf.close()
