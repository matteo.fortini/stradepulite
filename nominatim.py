from urllib import parse, request
import json

reverse_url =  "https://nominatim.openstreetmap.org/reverse?"
search_url = "https://nominatim.openstreetmap.org/search?"

def __perform_request(options, url):
    for opt in options.keys():
        if opt != "" and opt is not None:
            quoted_opt = parse.quote(opt)
            quoted_val = ""
            if options[opt] is not None and options[opt] != "":
                quoted_val = parse.quote(str(options[opt]))
            url += "{}={}&".format(quoted_opt, quoted_val)
    req = request.Request(url, headers={"User-Agent": "Mozilla"})
    return request.urlopen(req).read().decode('utf-8')

def reverse_geocode(lat, lon, osm_type=None, zoom=18, addressdetail=1, email=None):
    opts = {
        'lat': lat,
        'lon': lon,
        'format': 'json'
    }
    if osm_type is not None:
        opts['osm_type'] = osm_type
    if zoom is not None:
        opts['zoom'] = zoom
    if addressdetail is not None:
        opts['addressdetail'] = addressdetail
    if email is not None:
        opts['email'] = email
    return json.loads(__perform_request(opts, reverse_url))

def search(query, polygon=0, addressdetail=0, email=None):
    opts = {
        'q': query,
        'format': 'json'
    }
    if polygon is not None:
        opts['polygon'] = polygon
    if addressdetail is not None:
        opts['addressdetail'] = addressdetail
    if email is not None:
        opts['email'] = email
    return json.loads(__perform_request(opts, search_url))
