#!/usr/bin/env python3

import logging
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import configparser
import os
from pathlib import Path
import json
import strada
import nominatim
import db

logging.basicConfig(level=logging.INFO)

strade = db.DB("strade_db.json")

configFile = os.path.join(os.environ["HOME"], ".stradepulitebotrc")
config = configparser.ConfigParser()
config.read(configFile)

def set_if_missing(section, field, query):
    global config
    if section not in config:
        config.add_section(section)
    if field not in config[section] or len(config[section][field]) <= 0:
        config[section][field] = input(query)

set_if_missing("main", "telegram_token", "Telegram API token: ")

with open(configFile, 'w') as cf:
    config.write(cf)

def start(bot, update):
    update.message.reply_text(
            "Sono il bot che ti aiuta con la pulizia strade di Bologa.\n" +
            "I comandi che mi puoi dare sono:\n"+
            "\t/strada <nome strada>: ti dico i giorni in cui fanno la pulizia nelle strade che si chiamano <nome strada>\n" +
            "\t/help: ti ripeto questo messaggio\n"+
            "\nOppure puoi mandarmi la tua posizione e ti dico quando c'e` il lavaggio li` e ti avviso se il giorno in cui lo faranno\n"+
            "\nIo sono un bot ope source, mi puoi trovare qui: https://gitlab.com/mellotanica/stradepulite"
    )

def queryStrada(req):
    global strade

    resp = ""
    query = req.lower().split()
    hits = [[] for i in query]

    for s in strade.names():
        for q in range(len(query)):
            if query[q] in s.lower():
                hits[q] += strade.byName(s)

    lower = None
    minh = strade.length()

    for h in hits:
        if len(h) < minh:
            lower = h
            minh = len(h)

    if lower is not None:
        for ss in lower:
            resp += "{} {} {} ({})".format(ss.nome, ss.quale_text, ss.giorno_text, ss.next_real_day())
            if len(ss.descrizione_alt) > 0:
                resp += " -> " + ss.descrizione_alt
            resp += "\n"
    return resp

def getStrada(bot, update):
    req = update.message.text[len("/strada"):].strip()
    logging.info(req)
    if len(req) <= 0:
        update.message.reply_text("mi devi dire una strada pero`...")
        return
    resp = queryStrada(req)

    if len(resp) > 0:
        update.message.reply_text("il lavaggio verra` effettuato:\n"+resp)
    else:
        update.message.reply_text("non ho trovato nessuna strada che si chiama cosi`, prova a semplificare la richiesta")

def getLocation(bot, update):
    loc = nominatim.reverse_geocode(update.message.location.latitude, update.message.location.longitude)
    indirizzo = loc['address']['road']
    logging.info(indirizzo)
    resp = queryStrada(indirizzo)

    if len(resp) > 0:
        update.message.reply_text("ti trovi qui: "+str(indirizzo)+"\nil lavaggio verra` effettuato:\n"+resp)
    else:
        update.message.reply_text("ti trovi qui: "+str(indirizzo)+"\nnon ho trovato nessuna strada che si chiama cosi`")

def error(bot, update, error):
    logging.warning('Update "%s" caused error "%s"', update, error)

def snooze(bot, update):
    update.message.reply_text("disattivato i reminder per l'ultima posizione salvata")

updater = Updater(config['main']['telegram_token'])

dp = updater.dispatcher

dp.add_handler(CommandHandler("start", start))
dp.add_handler(CommandHandler("help", start))
dp.add_handler(CommandHandler("strada", getStrada))
dp.add_handler(CommandHandler("snooze", snooze))

dp.add_handler(MessageHandler(Filters.location, getLocation))

dp.add_error_handler(error)

updater.start_polling()

updater.idle()
